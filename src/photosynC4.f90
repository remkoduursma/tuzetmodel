!! SPA C4 photosynthesis subroutines
!! AUTHOR: Rhys Whitley
!! DATE: 03/11/2010
!! INSTITUTION: University of Technology, Sydney

	!--------------------------------------------------------------------
	!--------------------------------------------------------------------

		SUBROUTINE COLLATZ(Vcmax25,Rd,PAR,TLEAF,Ci,An)
	! Metabolic C4 assimilation rate from Collatz et al. 1992

			IMPLICIT NONE

			DOUBLE PRECISION Rd, PAR, TLEAF, Ci, Vcmax25
			DOUBLE PRECISION Vcmax
			DOUBLE PRECISION alpharf, theta, beta, K, M, An, A
			DOUBLE PRECISION TEMPARRHENIUSC4,QUADRATIC
			
			alpharf = 0.067			! mol/mol
			K = 0.7					! mol/m2/s
			theta = 0.83			! Curvature of quantum response curve, Collatz table2
			beta  = 0.93			! Collatz table 2

		! Apply appropriate temperature responses on the photosynthetic parameters (Massad et al. 2007)	
			!TEFFECTS: IF( TEMPON ) THEN
				Vcmax = Vcmax25 * TEMPARRHENIUSC4( DBLE(67294.0), DBLE(144568.0), DBLE(472.0), TLEAF )	
			!ELSE
			!	Vcmax = Vcmax25
			!ENDIF TEFFECTS

		! Rubisco and light limited capacity
			M = QUADRATIC(theta,-(alpharf*PAR+Vcmax),alpharf*PAR*Vcmax,DBLE(-1.0))	
		! M and CO2 limitation
			A = QUADRATIC(beta,-(M+K*Ci),M*K*Ci,DBLE(-1.0))
		! Net photosynthetic rate 	
			An = A-Rd
		! Return answer to user		
			RETURN

		END SUBROUTINE COLLATZ

	!--------------------------------------------------------------------
	!--------------------------------------------------------------------

		SUBROUTINE VONCAEMMERER(Vcmax25,Vpmax25,Jmax25,Kc25,Ko25,Kp25,Rd,PAR,TLEAF,Ci,AC,AJ,ALEAFHM)
	! C4 assimilation rate is calucated using quadratic solutions for the enzyme and light
	! limited rates of photosynthesis. All values and calculations are take from the
	! von Caemmerer and Furbank (1999) paper.
	! *Note: We express that Ci is equivalent to Cm

			IMPLICIT NONE
			
		! Function inputs
			DOUBLE PRECISION Rd, PAR, TLEAF, Ci
			DOUBLE PRECISION Vcmax25, Vpmax25, Jmax25, Kc25, Ko25, Kp25
			DOUBLE PRECISION Vcmax, Vpmax, Jmax, Kc, Ko, Kp
			DOUBLE PRECISION AC,AJ,ALEAFHM
			DOUBLE PRECISION TEMPARRHENIUSC4
			DOUBLE PRECISION QUADRATIC

			! Function variables					
			DOUBLE PRECISION a_c, b_c, c_c, a_j, b_j, c_j,		&
					gbs, low_gamstar, Oi, K, Rm, alpha, &
					Vp, Vpr, J, Q2, shape, x,			&
					A_enzyme, A_light,HMSHAPE,AC2,AJ2,AMIN
			
			HMSHAPE = 0.999
			
			alpha = 0.0				! Degree of PSII activity occuring in the bundle sheath cells (adjust between 0-1)
			Oi = 210.0				! Intercellular O2 equivalent to Om
			gbs = 0.003				! Bundle sheath conductance (mol m-2 s-1)
			low_gamstar = 1.93E-4	! Half the reciprocal for Rubisco specificity (NOT CO2 compensation point)
			x = 0.4					! Partitioning of the electron transport between mesophyll and bundle sheath cells
			shape = 0.7				! Shape of the hyperbolic response curve (typical value)
			Rm = 0.5*Rd				! Mitchondrial respiration from the mesophyll cells is half of Rd
			Vpr = 80.0				! PEP limiting regeneration rate
			K = Kc*(DBLE(1.0)+Oi/Ko)		! Combined Michelson-Menton constant for Ci and Oi

		! Apply appropriate temperature responses on the photosynthetic parameters (Massad et al. 2007)	
			!TEFFECTS: IF( TEMPON ) THEN
				Vcmax	= Vcmax25 * TEMPARRHENIUSC4( DBLE(67294.0), DBLE(144568.0), DBLE(472.0), TLEAF )	
				Vpmax	= Vpmax25 * TEMPARRHENIUSC4( DBLE(70373.0), DBLE(117910.0), DBLE(376.0), TLEAF )	
				Jmax	= Jmax25  * TEMPARRHENIUSC4( DBLE(77900.0), DBLE(191929.0), DBLE(627.0), TLEAF )
				Kc	= Kc25 * 2.1**((TLEAF-25)/10)  !TEMP_Q10( 2.1, TLEAF )
				Kp	= Kp25 * 2.1**((TLEAF-25)/10)  !TEMP_Q10( 2.1, TLEAF )
				Ko	= Ko25 * 2.1**((TLEAF-25)/10)  !TEMP_Q10( 2.1, TLEAF )
				
			!ELSE
			!Vcmax = Vcmax25
			!	Vpmax = Vpmax25
			!	Jmax = Jmax25
			!	Kc = Kc25
			!	Kp = Kp25
			!	Ko = Ko25
			!ENDIF TEFFECTS
			
		! PEP carboxylation rate
			Vp = MIN(Ci*Vpmax/(Ci+Kp),Vpr)	

		! Quadratic solution for enzyme limited C4 assimilation
			a_c = 1.0 - (alpha/0.047)*(Kc/Ko)
			b_c = -( (Vp-Rm+gbs*Ci) + (Vcmax-Rd) + gbs*K + alpha*low_gamstar/0.047*( low_gamstar*Vcmax+Rd*Kc/Ko ) )
			c_c = (Vcmax-Rd)*(Vp-Rm+gbs*Ci) - (Vcmax*gbs*low_gamstar*Oi + Rd*gbs*K)
			
			AC = QUADRATIC(a_c,b_c,c_c,DBLE(-1.0))	! Using negative quadratic solution
			
		! Non-rectangular hyperbola describing light effect on electron transport rate (J)
			Q2 = PAR*(1.0-0.15)/2.0
			J = QUADRATIC(shape,-(Q2+Jmax),(Q2*Jmax),DBLE(-1.0))
			
		! Quadratic solution for light-limited C4 assimilation       
			a_j = 1.0 - 7.0*low_gamstar*alpha/(3.0*0.047)
			b_j = -( (x*J/2.0-Rm+gbs*Ci) + ((1.0-x)*J/3.0-Rd) + gbs*(7.0*low_gamstar*Oi/3.0)	&
					+ alpha*low_gamstar/0.047*((1.0-x)*J/3.0+Rd) )
			c_j = ( (x*J/2.0-Rm+gbs*Ci)*((1.0-x)*J/3.0-Rd) - gbs*low_gamstar*Oi*((1.0-x)*J/3.0-7.0*Rd/3.0) )
			
			AJ = QUADRATIC(a_j,b_j,c_j,DBLE(-1.0))	! Using negative quadratic solution
			
		! The minimum between enzyme and light limited respiration rates is the actual rate
		AMIN = MIN(AC,AJ)
		
		! Hyperbolic minimum; use for optimization to avoid discontinuity.
        ! HMSHAPE determines how smooth the transition is.
        AC2 = AC +RD
        AJ2 = AJ+RD
		ALEAFHM = (AC2+AJ2 - sqrt((AC2+AJ2)**2-4*HMSHAPE*AC2*AJ2))/(2*HMSHAPE) - RD	
		
		RETURN		
		END SUBROUTINE VONCAEMMERER

	!--------------------------------------------------------------------
	!--------------------------------------------------------------------
	
	DOUBLE PRECISION FUNCTION TEMPARRHENIUSC4(TLEAF,Ea,Hd,DS)
! Arrhenius function use with C4 metabolic parameters as described by
! Massad et al. 2007

IMPLICIT NONE

	DOUBLE PRECISION lt, Ea, Hd, DS
	DOUBLE PRECISION	R, Tk, fTk
	DOUBLE PRECISION TLEAF
	
	R = 8.3144		! Universal gas constant
	Tk = TLEAF + 273.15		! Convert leaf temperature from degrees to Kelvin
	
! Arrhenius expression as used by Massad et al. 2007	
	TEMPARRHENIUSC4 = EXP( Ea*(Tk-298.0)/(298.0*R*Tk) )				&
			* (1.0 + EXP( (298.0*DS-Hd)/(298.0*R) ))	&
			/ (1.0 + EXP( (Tk*DS-Hd)/(Tk*R) )) 

	
END FUNCTION TEMPARRHENIUSC4

!-------------------------------------------------------------------

DOUBLE PRECISION FUNCTION QUADRATIC(a, b, c, signs) 
! General quadratic solution function

IMPLICIT NONE
	
	DOUBLE PRECISION a, b, c, signs
	DOUBLE PRECISION x

! Is the quadratic solution negative or positive in sign	
	IF(signs < 0.0) THEN
		x = (-b-SQRT(b**2.0-4.0*a*c))/(2.0*a)	! Negative quadratic equation
	ELSE
		x = (-b+SQRT(b**2.0-4.0*a*c))/(2.0*a)	! Positive quadratic equation
	END IF
	
! Return answer to user		
	QUADRATIC = x

END FUNCTION QUADRATIC
